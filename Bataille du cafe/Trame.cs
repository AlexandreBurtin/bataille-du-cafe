﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Bataille_du_cafe
{
    public class Trame
    {
        // Se connecte et decode une trame reçu pour la renvoyer sous forme d'une liste d'entier
        // IN: 
        // OUT: liste d'entier correspondant à la trame
        public static List<Cases> DecodageTrame(Socket p_socket)
        {

            byte[] v_tableau;
            List<int> v_intTrame = new List<int>();
            List<Cases> v_caseTrame = new List<Cases>();
            v_tableau = CallServerForCarte(p_socket);
            v_intTrame = ConversionByteToInt(v_tableau);
            v_caseTrame = ConversionIntToCases(v_intTrame);
            return v_caseTrame;
        }
        /// <summary>
        /// Fonction qui permet de recevoir les données depuis le serveur
        /// </summary>
        /// <param name="p_socket">Socket de connexion utilisé</param>
        /// <param name="p_tableau">Tableau d'octet servant a recevoir les données</param>
        public static byte[] CallServerForCarte(Socket p_socket)
        {
            byte[] v_tableau = new byte[259];
            int v_size;
            v_size = p_socket.Receive(v_tableau);
            System.Array.Resize<Byte>(ref v_tableau, v_size);
            return v_tableau;

        }
        public static byte[] CallServer(Socket p_socket)
        {
            byte[] v_tableau = new byte[4];
            int v_size;
            v_size = p_socket.Receive(v_tableau);
            System.Array.Resize<Byte>(ref v_tableau, v_size);
            return v_tableau;

        }
        /// <summary>
        /// Fonction qui permet d'envoyer les données au serveur
        /// </summary>
        /// <param name="p_socket">Socket de connexion utilisé</param>
        /// <param name="p_tableau">Tableau d'octet à envoyer au serveur</param>
        public static void SendServer(Socket p_socket, byte[] p_tableau)
        {
            p_socket.Send(p_tableau);
        }
        // Se connecte au serveur et stock les valeurs dans un tableau, renvoi le nombre d'octets reçu*
        // IN: tableau d'octet qui contiendra les valeurs de la trame
        // OUT: entier qui correponds au nombre d'octet reçu
        public static Socket TrameConnection()
        {

            Socket v_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            v_socket.Connect("127.0.0.1", 1213);
            return v_socket;
        }
        //Convertie le tableau d'octet en liste d'entier et supprime le caractère ":"
        //IN: tableau d'octet a convertir
        //OUT: Liste d'entier qui correspond à la trame 
        private static List<int> ConversionByteToInt(byte[] p_tableau)
        {
            List<string> v_carte = new List<string>();
            List<int> v_intcarte = new List<int>();
            v_carte = DecompositionCaractere(v_carte, p_tableau);
            v_intcarte = v_carte.ConvertAll(int.Parse);
            return v_intcarte;
        }
        //Convertie le tableau d'octet en une liste de chaine de caractère et supprime le caractère ":"
        // IN: tableau d'octet a convertir
        // OUT: liste de string qui correponds aux tableau d'octet
        private static List<string> DecompositionCaractere(List<string> p_carte, byte[] p_tableau)
        {
            int v_iteration = 0;
            while (v_iteration < p_tableau.Count())
            {
                if (p_tableau[v_iteration] == 58 || p_tableau[v_iteration] == 124)
                    v_iteration++;
                else
                    CompteurOctet(p_tableau, ref v_iteration, p_carte);
            }
            return p_carte;
        }
        //defini combien d'octet sont convertie entre chaque caractère ":"
        // IN:Tableau d'octet a convertir,liste de string qui va contenir le tableau
        // OUT:
        private static void CompteurOctet(byte[] p_tableau, ref int p_iteration, List<string> p_carte)
        {
            if (p_tableau[p_iteration + 1] == 58 || p_tableau[p_iteration + 1] == 124)
            {
                p_carte.Add(ASCIIEncoding.ASCII.GetString(p_tableau, p_iteration, 1));
                p_iteration++;
            }
            else
            {
                p_carte.Add(ASCIIEncoding.ASCII.GetString(p_tableau, p_iteration, 2));
                p_iteration += 2;
            }
        }
        // Convertie une liste d'entier en liste d'objet Cases
        // IN: Liste d'entier
        //OUT: Liste de Cases
        private static List<Cases> ConversionIntToCases(List<int> p_trame)
        {
            int[] v_tableau = new int[7];
            List<Cases> v_listeCase = new List<Cases>();
            Cases v_cellule;
            for (int v_iteration = 0; v_iteration < p_trame.Count(); v_iteration++)
            {
                v_tableau = ConversionIntToTab(p_trame[v_iteration]);
                v_cellule = new Cases(v_tableau);
                v_listeCase.Add(v_cellule);
            }
            return v_listeCase;
        }
        //Convertie un entier en tableau de puissance de 2(utiliser pour definir les Cases du plateau) sous forme [1,0,1,0,1,0,0]
        // IN:Entier 
        //OUT: tableau de puissance de 2 
        private static int[] ConversionIntToTab(int p_valeur)
        {
            int v_soustraction = 64;
            int[] v_tableau = new int[7];
            for (int v_iteration = 0; v_iteration < 7; v_iteration++)
            {
                v_tableau[v_iteration] = Soustraction(ref p_valeur, ref v_soustraction);
            }
            if (v_tableau[0] == 0 && v_tableau[1] == 0)
            {
                v_tableau[2] = 1;
            }
            return v_tableau;
        }
        // effectue une soustraction avec les paramètres envoyé et retourne 1 ou 0 si le resultat est supérieur à 0 
        // IN: 2 entier valeur et valeur a soustraire
        //OUT: Entier 1 ou 0
        private static int Soustraction(ref int p_valeur, ref int p_soustraction)
        {
            if (p_valeur - p_soustraction >= 0)
            {
                p_valeur = p_valeur - p_soustraction;
                p_soustraction = p_soustraction / 2;
                return 1;
            }
            else
            {
                p_soustraction = p_soustraction / 2;
                return 0;
            }
        }
    }
}