﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bataille_du_cafe
{
    public class Score
    {
        private int[,] tableauScore;
        // Initialise un tableau 10x10 utilisable pour la logique de l'IA
        // IN: 
        //OUT: 
        public Score()
        

        {
            this.tableauScore = new int[10,10];
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    this.tableauScore[ligne, colonne] = 0;
                }
            }
        }
        public int GetValue(int p_ligne,int p_colonne)
        {
            return this.tableauScore[p_ligne, p_colonne];
        }
        
        public void SetValue(int p_ligne,int p_colonne,int value)
        {
            this.tableauScore[p_ligne, p_colonne] = value;
        }
    }
}
