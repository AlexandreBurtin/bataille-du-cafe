﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bataille_du_cafe
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            List<Cases> v_DataTrame;
            Carte v_carte;
            InitializeComponent();
            Socket v_socket = Trame.TrameConnection();
            v_DataTrame = Trame.DecodageTrame(v_socket);
            v_carte = Carte.CreationCarte(v_DataTrame);
            Score v_tabScore = new Score();
            IA.PartieContreServeur(v_carte, v_socket, v_tabScore);
        }


    }
    
}
