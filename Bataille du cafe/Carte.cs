﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bataille_du_cafe
{
    public class Carte
    {
        private Cases[,] m_carte;
        public Carte()
        {
            this.m_carte = new Cases[10, 10];
        }
        private void SetCarte(int p_ligne,int p_colonne,Cases p_cases)
        {
            this.m_carte[p_ligne, p_colonne] = p_cases;
        }
        public Cases GetCases(int p_ligne, int p_colonne)
        {
            return this.m_carte[p_ligne,p_colonne];
        }
        public static Carte CreationCarte(List<Cases> p_listeCases)
        {
            Carte v_carte = new Carte();
            InsertionCaseInCarte(v_carte,p_listeCases);
            InitialisationParcelle(v_carte);
            return v_carte;
        }
        private static void InsertionCaseInCarte(Carte p_carte,List<Cases> p_listeCases)
        {
            for (int v_ligne = 0; v_ligne < 10; v_ligne++)
            {
                for (int v_colonne = 0; v_colonne < 10; v_colonne++)
                {
                    p_carte.SetCarte(v_ligne, v_colonne,p_listeCases[(v_ligne*10)+v_colonne]) ;
                }
            }
        }
        private static void InitialisationParcelle(Carte p_carte)
        {
            int v_iteration = 0;
            for(int v_ligne=0;v_ligne<10;v_ligne++)
            {
                for (int v_colonne = 0; v_colonne < 10; v_colonne++)
                {
                    if (p_carte.GetCases(v_ligne,v_colonne).GetNumeroParcelle()==0)
                    {
                        v_iteration++;
                        AjoutParcelle(p_carte, v_iteration,v_ligne,v_colonne);
                    }
                   
                }
            }
        }
        private static void AjoutParcelle(Carte p_carte,int p_iteration,int p_ligne,int p_colonne)
        {
            if(p_carte.GetCases(p_ligne, p_colonne).GetNumeroParcelle()==0)
            {
                p_carte.GetCases(p_ligne, p_colonne).SetNumeroParcelle(p_iteration);
                if (p_carte.GetCases(p_ligne, p_colonne).GetMurBas() == false) AjoutParcelle(p_carte, p_iteration, p_ligne + 1, p_colonne);
                if (p_carte.GetCases(p_ligne, p_colonne).GetMurDroite() == false) AjoutParcelle(p_carte, p_iteration, p_ligne, p_colonne + 1);
                if (p_carte.GetCases(p_ligne, p_colonne).GetMurHaut() == false) AjoutParcelle(p_carte, p_iteration, p_ligne - 1, p_colonne);
                if (p_carte.GetCases(p_ligne, p_colonne).GetMurGauche() == false) AjoutParcelle(p_carte, p_iteration, p_ligne, p_colonne - 1);
            }
        }
    }
}
