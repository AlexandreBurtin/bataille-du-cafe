﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bataille_du_cafe
{
    public class Cases
    {
        private int m_numeroParcelle;
        private bool m_murHaut;
        private bool m_murBas;
        private bool m_murDroite;
        private bool m_murGauche;
        private bool m_foret;
        private bool m_plaine;
        private bool m_mer;
        private string m_caracteristique;
        private char graine;
        // Construit un objet Cases qui represente une cellule de la carte avec les caracteristique envoyé en parametres (1 pour true et 0 pour false)
        // IN:Tableau de 7 cases contenant comme valeur 0 ou 1
        public Cases(int[] p_tableau)
        {
            this.m_numeroParcelle = 0; // Est defini pendant la création de la carte 
            this.m_mer = ConversionIntToBool(p_tableau[0]); // defini si la cellule est une cellule Mer
            this.m_foret = ConversionIntToBool(p_tableau[1]);// defini si la cellule est une cellule foret
            this.m_plaine = ConversionIntToBool(p_tableau[2]);// defini si la cellule est une cellule plaine
            this.m_murDroite = ConversionIntToBool(p_tableau[3]);// defini si la cellule contient un mur sur le coté droit
            this.m_murBas = ConversionIntToBool(p_tableau[4]);// defini si la cellule contient un mur sur le coté base
            this.m_murGauche = ConversionIntToBool(p_tableau[5]);// defini si la cellule contient un mur sur le coté gauche
            this.m_murHaut = ConversionIntToBool(p_tableau[6]);// defini si la cellule contient un mur sur le coté haut
            this.m_caracteristique = String.Join("", p_tableau); // contient une chaine de caractère sous la forme "0101101" utilisé pour l'affichage
            this.graine = 'N';
        }
        //Convertie un entier en booléen (1 pour true et 0 pour false)
        //IN: Entier à convertir 
        //OUT:booléen 
        private bool ConversionIntToBool(int p_valeur)
        {
            if(p_valeur==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int GetNumeroParcelle()
        {
            return this.m_numeroParcelle;
        }
        public void SetNumeroParcelle(int p_valeur)
        {
            this.m_numeroParcelle = p_valeur;
        }
        public bool GetMurHaut()
        {
            return this.m_murHaut;
        }
        public bool GetMurBas()
        {
            return this.m_murBas;
        }
        public bool GetMurDroite()
        {
            return this.m_murDroite;
        }
        public bool GetMurGauche()
        {
            return this.m_murGauche;
        }
        public bool GetForet()
        {
            return this.m_foret;
        }
        public bool GetPlaine()
        {
            return this.m_plaine;
        }
        public bool GetMer()
        {
            return this.m_mer;
        }
        public string GetCaracteristique()
        {
            return this.m_caracteristique;
        }
        public char GetGraine()
        {
            return this.graine;
        }
        public void SetGraine(char p_joueur)
        {
            this.graine = p_joueur;
        }
    }
}

