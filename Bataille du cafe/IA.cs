﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Bataille_du_cafe
{
    public static class IA
    {
        // Fonction qui s'occupe du déroulement de la partie 
        // IN : La carte de jeu , Le socket et le tableau de Score
        // OUT: 
        public static void PartieContreServeur(Carte p_carte,Socket p_socket,Score p_tableauScore)
        {
            bool v_rejouer = false;
            PremierTourIA(p_carte, p_socket, p_tableauScore);
            PremierTourServeur(p_carte, p_socket, p_tableauScore);
            do
            {
                TourIA(p_carte, p_socket, p_tableauScore);
                v_rejouer = TourServeur(p_carte, p_socket, p_tableauScore);
            } while (v_rejouer == true);

        }
        // Fonction qui s'occupe du premier tour de l'IA
        // IN : La carte de jeu , Le socket et le tableau de Score
        // OUT: 
        private static void PremierTourIA(Carte p_carte,Socket p_socket,Score p_tableauScore)
        {
            byte[] v_positionIAEnByte;
            byte[] v_validationPosition;
            int[] v_positionIA;
            do
            {
                v_positionIA = GenerationPositionAleatoire();
            }while(p_carte.GetCases(v_positionIA[0], v_positionIA[1]).GetPlaine() == false);
            v_positionIAEnByte = ConversionIntEnByte(v_positionIA);
            Trame.SendServer(p_socket, v_positionIAEnByte);
            v_validationPosition = Trame.CallServer(p_socket);
            if (v_validationPosition[0] == 86)
            {
                p_carte.GetCases(v_positionIA[0], v_positionIA[1]).SetGraine('A');
                p_tableauScore.SetValue(v_positionIA[0], v_positionIA[1], 1);
            }
        }
        // Fonction qui s'occupe de generation une position X et Y aléatoire
        // IN :
        // OUT: un tableau d'entier contenant les positions X et Y  
        private static int[] GenerationPositionAleatoire()
        {
            Random v_aleatoire = new Random();
            int[] v_position = new int[2];
            v_position[0] = v_aleatoire.Next(10);
            v_position[1] = v_aleatoire.Next(10);
            return v_position;
        }
        // Fonction qui convertie une position en tableau d'octets
        // IN : un tableau d'entier contenant les position X et Y
        // OUT: renvoi un tableau d'octet de valeur [A,:,X,Y]
        private static byte[] ConversionIntEnByte(int[] p_positionIA)
        {
            string v_textPosition = "A:" + Convert.ToString(p_positionIA[0]) + Convert.ToString(p_positionIA[1]);
            byte[] v_Byteposition = ASCIIEncoding.ASCII.GetBytes(v_textPosition);
            return v_Byteposition;
        }
        // Fonction qui s'occupe du premier tour du serveur
        // IN : La carte de jeu , Le socket et le tableau de Score
        // OUT: 
        private static void PremierTourServeur(Carte p_carte, Socket p_socket, Score p_tableauScore)
        {
            byte[] v_positionServeurEnByte;
            byte[] v_rejouer;
            int[] v_positionServeur = new int[2];
            v_positionServeurEnByte = Trame.CallServer(p_socket);
            v_positionServeur[0] =int.Parse(ASCIIEncoding.ASCII.GetString(v_positionServeurEnByte, 2, 1));
            v_positionServeur[1] = int.Parse(ASCIIEncoding.ASCII.GetString(v_positionServeurEnByte, 3, 1));
            p_carte.GetCases(v_positionServeur[0], v_positionServeur[1]).SetGraine('B');
            p_tableauScore.SetValue(v_positionServeur[0], v_positionServeur[1], 2);
            v_rejouer = Trame.CallServer(p_socket);
        }

        // Fonction qui s'occupe du premier tour de l'IA
        // IN : La carte de jeu , Le socket et le tableau de Score
        // OUT: 
        private static void TourIA(Carte p_carte, Socket p_socket, Score p_tableauScore)
        {
            int[] v_positionIA = new int[2];
            byte[] v_positionIAEnByte;
            byte[] v_validationPosition;
            v_positionIA = RecherchePositionValide(p_carte, p_tableauScore);
            v_positionIAEnByte=ConversionIntEnByte(v_positionIA);
            Trame.SendServer(p_socket, v_positionIAEnByte);
            v_validationPosition = Trame.CallServer(p_socket);
            if (v_validationPosition[0] == 86)
            {
                p_carte.GetCases(v_positionIA[0], v_positionIA[1]).SetGraine('A');
                p_tableauScore.SetValue(v_positionIA[0], v_positionIA[1], 1);
            }
        }
        // Recherche Horizontalement puis verticalement une position valide à jouer
        // IN: La carte , le tableau de score
        //OUT: Retourne un tableau d'entier avec une position jouable [X,Y]
        private static int[] RecherchePositionValide(Carte p_carte, Score p_tabScore)
        {
            int v_PosX = 0;
            int v_PosY = 0;
            int[] v_positionIA = new int[2];
            int[] v_dernierePose = RecuperationDernierePose(p_carte, p_tabScore);
            if (RecherchePosX(p_carte, ref v_PosX, ref v_PosY, v_dernierePose) == true)
            {
                v_positionIA[0] = v_PosX;
                v_positionIA[1] = v_PosY;
                return v_positionIA;
            }
            else
            {
                RecherchePosY(p_carte, ref v_PosX, ref v_PosY, v_dernierePose);
                v_positionIA[0] = v_PosX;
                v_positionIA[1] = v_PosY;
                return v_positionIA;
            }

        }
        // Recupère la dernière pose du serveur via le tableau de score
        // IN: La carte , le tableau de score 
        //OUT: Returne un tableau d'entier contenant la dernière position du serveur [X,Y]
        private static int[] RecuperationDernierePose(Carte p_carte, Score p_tabScore)
        {
            int[] v_dernierePose = new int[3];
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (p_tabScore.GetValue(ligne, colonne) == 2)
                    {
                        v_dernierePose[0] = p_carte.GetCases(ligne, colonne).GetNumeroParcelle();
                        v_dernierePose[1] = ligne;
                        v_dernierePose[2] = colonne;
                    }
                }
            }
            return v_dernierePose;
        }
        // Recherche horizontalement une position de jeu valide pour l'IA
        // IN: La carte , La position X , la position Y et la dernière pose du serveur
        //OUT: Un booléen qui renvoie si une position en horizontal est valide 
        private static bool RecherchePosX(Carte p_carte, ref int p_posX, ref int p_posY, int[] p_dernierePose)
        {
            int v_compteur = 0;
            bool v_checkPose = false;
            do
            {
                if (p_carte.GetCases(v_compteur, p_dernierePose[2]).GetPlaine() == true &&
                    p_carte.GetCases(v_compteur, p_dernierePose[2]).GetGraine() == 'N' &&
                    p_carte.GetCases(v_compteur, p_dernierePose[2]).GetNumeroParcelle() != p_dernierePose[0])
                {
                    p_posX = v_compteur;
                    p_posY = p_dernierePose[2];
                    v_checkPose = true;
                }
                v_compteur++;
            } while (v_compteur != 10 && v_checkPose == false);

            return v_checkPose;
        }
        // Recherche verticalement une position de jeu valide pour l'IA
        // IN: La carte , la position X , la position Y et la dernière pose du serveur
        //OUT:
        private static void RecherchePosY(Carte p_carte, ref int p_posX, ref int p_posY, int[] p_dernierePose)
        {
            int v_compteur = 0;
            bool v_checkPose = false;
            do
            {
                if (p_carte.GetCases(p_dernierePose[1], v_compteur).GetPlaine() == true &&
                    p_carte.GetCases(p_dernierePose[1], v_compteur).GetGraine() == 'N' &&
                    p_carte.GetCases(p_dernierePose[1], v_compteur).GetNumeroParcelle() != p_dernierePose[0])
                {
                    p_posX = p_dernierePose[1];
                    p_posY = v_compteur;
                    v_checkPose = true;
                }
                v_compteur++;
            } while (v_compteur != 11 && v_checkPose == false);
        }
        // S'occupe de recuperer la position du serveur et de mettre a jours le tableau de score
        // IN:  La carte de jeu , Le socket et le tableau de Score
        //OUT: Un booléen si il est possible de rejouer ou non
        private static bool TourServeur(Carte p_carte, Socket p_socket, Score p_tableauScore)
        {
            byte[] v_positionEnByte;
            int[] v_position = new int[2];
            byte[] v_rejouer;

            v_positionEnByte = Trame.CallServer(p_socket);
            if (v_positionEnByte[0] == 66)
            {
                v_position[0] = int.Parse(ASCIIEncoding.ASCII.GetString(v_positionEnByte, 2, 1));
                v_position[1] = int.Parse(ASCIIEncoding.ASCII.GetString(v_positionEnByte, 3, 1));
                MiseAJourScore(p_tableauScore);
                p_carte.GetCases(v_position[0], v_position[1]).SetGraine('B');
                p_tableauScore.SetValue(v_position[0], v_position[1], 2);
                v_rejouer = Trame.CallServer(p_socket);
                if (v_rejouer[0]==69)
                {
                    return true;
                }
            }
            return false;
        }
        // Change tout les valeurs qui sont égale a 2 dans le tableau de score pour les mettres à 1 
        // IN: Tableau de score
        //OUT:
        private static void MiseAJourScore(Score p_tabScore)
        {
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (p_tabScore.GetValue(ligne, colonne) == 2)
                    {
                        p_tabScore.SetValue(ligne, colonne, 1);
                    }
                }
            }

        }
    }

}
