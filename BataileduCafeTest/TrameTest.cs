﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bataille_du_cafe;
using System.Linq;

namespace BataileduCafeTest
{
    /// <summary>
    /// Description résumée pour UnitTest1
    /// </summary>
    [TestClass]
    public class TrameTest
    {
        //Verification que la récupération des valeurs est opérationelle en comptant le nombre de valeur reçu
        //La trame envoyé par le serveur ("51.91.120.237", 1212) renvoi 100 valeurs 
        [TestMethod]
        public void TestConversionByteToInt()
        {
            List<Cases> v_liste = new List<Cases>();
            int[] v_tab = new int[7];
            Cases v_cases = new Cases(v_tab);
            v_liste = Trame.DecodageTrame();
            Assert.AreEqual(v_liste.Count(), 100);
            for(int v_iteration=0;v_iteration<100;v_iteration++)
            {
                Assert.AreEqual(v_liste[v_iteration].GetType(), v_cases.GetType());
            }
        }

    }
}
