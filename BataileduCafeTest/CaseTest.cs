﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bataille_du_cafe;

namespace BataileduCafeTest
{
    [TestClass]
    public class CaseTest
    {
        [TestMethod]
        public void TestConstructeur()
        {
            //Nous testons la création d'une case forêt avec 1 mur droit et un mur Haut
            int[] v_tab = new int[7] { 0, 1, 0, 1, 0, 0, 1 };
            Cases v_cellule = new Cases(v_tab);
            Assert.AreEqual(v_cellule.GetMurBas(), false);
            Assert.AreEqual(v_cellule.GetMurHaut(), true);
            Assert.AreEqual(v_cellule.GetMurDroite(), true);
            Assert.AreEqual(v_cellule.GetMurGauche(), false);
            Assert.AreEqual(v_cellule.GetMer(), false) ;
            Assert.AreEqual(v_cellule.GetForet(), true);
            Assert.AreEqual(v_cellule.GetPlaine(), false);
            // Nous testons cette fois la création d'une case plaine avec 4 murs
            int[] v_tab2 = new int[7] { 0, 0, 1, 1, 1, 1, 1 };
            Cases v_cellule2 = new Cases(v_tab2);
            Assert.AreEqual(v_cellule2.GetMurBas(), true);
            Assert.AreEqual(v_cellule2.GetMurHaut(), true);
            Assert.AreEqual(v_cellule2.GetMurDroite(), true);
            Assert.AreEqual(v_cellule2.GetMurGauche(), true);
            Assert.AreEqual(v_cellule2.GetMer(), false);
            Assert.AreEqual(v_cellule2.GetForet(), false);
            Assert.AreEqual(v_cellule2.GetPlaine(), true);
        }
    }
}
